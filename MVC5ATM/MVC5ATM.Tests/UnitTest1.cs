﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC5ATM.Controllers;
using System.Web.Mvc;
using MVC5ATM.Models;

namespace MVC5ATM.Tests
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void FooActionReturnsAboutView()
		{
			var homeController = new HomeController();
			var result = homeController.Foo() as ViewResult;

			Assert.AreEqual("About", result.ViewName);
		}

		[TestMethod]
		public void ContactFormSaysThanks()
		{
			var homeController = new HomeController();
			var result = homeController.Contact("My Thanks Message") as ViewResult;

			Assert.AreEqual("Thank's we got your message", result.ViewBag.Message);
			Assert.IsNotNull(result.ViewBag.Message);
		}

		[TestMethod]
		public void BalanceIsCorrectAfterDeposit()
		{
			var fakeDb = new FakeApplicationDbContext();
			fakeDb.Checkingaccounts = new FakeDbSet<CheckingAccount>();

			var checkingAccount = new CheckingAccount { Id = 1, AccountNumber = "000123TEST", Balance = 0 };
			fakeDb.Checkingaccounts.Add(checkingAccount);
			fakeDb.Transactions = new FakeDbSet<Transaction>();

			var transactionController = new TransactionController(fakeDb);
			transactionController.Deposit(new Transaction { CheckingAccountId = 1, Amount = 25 });
			//checkingAccount.Balance = 25; - replaced by new method in checkingAccountService method update
			Assert.AreEqual(25, checkingAccount.Balance);
		}
	}
}
