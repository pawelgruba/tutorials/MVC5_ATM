namespace MVC5ATM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pinhashhasbeenadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PinHash", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "PinHash");
        }
    }
}
