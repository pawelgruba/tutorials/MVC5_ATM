namespace MVC5ATM.Migrations
{
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;
	using MVC5ATM.Models;
	using Microsoft.AspNet.Identity.EntityFramework;
	using Microsoft.AspNet.Identity;
	using MVC5ATM.Services;

	internal sealed class Configuration : DbMigrationsConfiguration<MVC5ATM.Models.ApplicationDbContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(MVC5ATM.Models.ApplicationDbContext context)
		{
			//  This method will be called after migrating to the latest version.

			//  You can use the DbSet<T>.AddOrUpdate() helper extension method 
			//  to avoid creating duplicate seed data. E.g.
			//
			//    context.People.AddOrUpdate(
			//      p => p.FullName,
			//      new Person { FullName = "Andrew Peters" },
			//      new Person { FullName = "Brice Lambson" },
			//      new Person { FullName = "Rowan Miller" }
			//    );
			//

			SeedRoles(context);
			SeedTransactions(context);
		}

		private void SeedTransactions(ApplicationDbContext context)
		{
			if (context.Transactions.Count() < 10)
			{
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 55, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 765, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 7545, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = , CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
				context.Transactions.Add(new Transaction { Amount = 75, CheckingAccountId = 1 });
			}
		}

		private void SeedRoles(ApplicationDbContext context)
		{
			var userStore = new UserStore<ApplicationUser>(context);
			var userManager = new UserManager<ApplicationUser>(userStore);

			if (!context.Users.Any(w => w.UserName == "admin@mvcatm.com"))
			{
				var user = new ApplicationUser() { UserName = "admin@mvcatm.com", Email = "admin@mvcatm.com" };
				userManager.Create(user, "P@ssword!");

				var service = new CheckingAccountService(context);

				service.CreateCheckingAccount("admin", "user", user.Id, 1000);

				

				if (!context.Roles.Any(w => w.Name == "Admin"))
				{
					context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
					{
						Name = "Admin"
					});
				}

				context.SaveChanges();

				userManager.AddToRole(user.Id, "Admin");

			}
		}
	}
}
