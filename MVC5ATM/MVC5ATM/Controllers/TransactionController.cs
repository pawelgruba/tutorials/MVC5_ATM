﻿using MVC5ATM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVC5ATM.Services;

namespace MVC5ATM.Controllers
{
	[Authorize]
	public class TransactionController : Controller
	{
		private IApplicationDbContext db;

		public TransactionController()
		{
			db = new ApplicationDbContext();
		}

		public TransactionController(IApplicationDbContext dbContext)
		{
			db = dbContext;
		}
		
		[HttpGet]
		public ActionResult Deposit(int checkingAccountId)
		{
			return View();
		}

		[HttpPost]
		public ActionResult Deposit(Transaction transaction)
		{
			if (ModelState.IsValid)
			{
				//var currentUserId = User.Identity.GetUserId();
				//var account = db.Checkingaccounts.Single(w => w.ApplicationUserId == currentUserId);

				//account.Balance += transaction.Amount;
				db.Transactions.Add(transaction);
				db.SaveChanges();

				var service = new CheckingAccountService(db);
				service.UpdateBalance(transaction.CheckingAccountId);


				return RedirectToAction("Index", "Home");
			}
			return View();
		}
	}
}
