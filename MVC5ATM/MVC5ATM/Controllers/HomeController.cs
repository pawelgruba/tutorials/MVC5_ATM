﻿using MVC5ATM.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVC5ATM.Models;
using Microsoft.AspNet.Identity.Owin;

namespace MVC5ATM.Controllers
{
	public class HomeController : Controller
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		[Authorize]
		//[ActionExecutionLoger] - rejestracja filtra przeniesiona do FilterConfig aby móc go stosować globalnie
		[OutputCache(Duration =120,VaryByParam ="???")]
		[HandleError(View = "MathError", ExceptionType = typeof(DivideByZeroException))]
		public ActionResult Index()
		{
			var userId = User.Identity.GetUserId();
			var checkingAccountId = db.Checkingaccounts.Where(w => w.ApplicationUserId == userId).First().Id;
			ViewBag.CheckingAccountId = checkingAccountId;

			var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			var user = manager.FindById(userId);
			ViewBag.Pin = user.Pin;

			return View();
		}

		[ActionName("about-this-atm")]
		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View("About"); //need to pass right name of View file, without it, mvc will looking for view called "about-this-atm"
		}

		public ActionResult Contact()
		{
			//ViewBag value is available only for view, if we redirect to another action, ViewBag value won't be available.
			//TempData przetrwa przekierowania
			ViewBag.Message = "Dawaj message";

			return View();
		}

		[HttpPost]
		public ActionResult Contact(string message)
		{
			//ViewBag value is available only for view, if we redirect to another action, ViewBag value won't be available.
			//TempData przetrwa przekierowania
			ViewBag.Message = "Thank's we got your message";

			return PartialView("_ContactThanks");
		}


		//create alias to "About" message
		public ActionResult Foo()
		{
			//throw new Exception(); //only for tests
			return View("About"); //use "Foo" as alias to "About" page
		}

		public ActionResult Serial(string letterCase)
		{
			var serial = "ASP.NET.ATM.SIMULATOR";

			if (letterCase == "lower")
				return Content(serial.ToLower());
			//else return Content(serial);

			return Json(new { name = "serial", value = serial },JsonRequestBehavior.AllowGet);
		}

		public ActionResult SayThanks(string question)
		{
			var answer = "it comes from controller";

			return Json(new { name = "serial", value = answer }, JsonRequestBehavior.AllowGet);
		}


	}
}