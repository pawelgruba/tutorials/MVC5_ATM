﻿using Microsoft.AspNet.Identity;
using MVC5ATM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5ATM.Controllers
{
	[Authorize]
    public class CheckingAccountController : Controller
    {
		private ApplicationDbContext _db = new ApplicationDbContext();


        // GET: CheckingAccount/Details
        public ActionResult Details(int checkingAccountId)
        {
			var userId = User.Identity.GetUserId();
			var account = _db.Checkingaccounts.Where(w => w.ApplicationUserId == userId).First();
            return View(account);
        }

		[Authorize(Roles ="Admin")]
		public ActionResult DetailsForAdmin(int id)
		{
			var checkingAccount = _db.Checkingaccounts.Find(id);
			return View("Details",checkingAccount);
		}

		[Authorize(Roles ="Admin")]
		public ActionResult List()
		{
			return View(_db.Checkingaccounts.ToList());
		}

        // GET: CheckingAccount/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CheckingAccount/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CheckingAccount/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CheckingAccount/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CheckingAccount/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CheckingAccount/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		public ActionResult Statement(int id)
		{
			var checkingAccount = _db.Checkingaccounts.Find(id);
			return View(checkingAccount.Transactions.ToList());
		}
	}
}
