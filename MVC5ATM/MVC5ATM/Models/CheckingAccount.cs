﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC5ATM.Models
{
	public class CheckingAccount
	{
		public int Id { get; set; }

		[Display(Name="Account #")]
		[Required]
		//[StringLength(10,MinimumLength =6)]
		[RegularExpression(@"\d{6,10}",ErrorMessage ="This number is not valid")]
		//for db definition:
		[StringLength(10)]
		[Column(TypeName ="varchar")]
		public string AccountNumber { get; set; }

		[Required]
		[Display(Name ="First Name")]
		public string Firstname { get; set; }

		[Required]
		[Display(Name ="Last Name")]
		public string LastName { get; set; }

		public string Name
		{
			get
			{
				return string.Format("{0} {1}", this.Firstname, this.LastName);
			}
		}

		[Display(Name ="Account Balance")]
		[DataType(DataType.Currency)]
		public decimal Balance { get; set; }

		public virtual ApplicationUser User { get; set; }

		[Required]
		public string ApplicationUserId { get; set; }

		public virtual ICollection<Transaction> Transactions { get; set; }
	}
}