﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace MVC5ATM.Models
{
	public class Transaction
	{
		public int Id { get; set; }

		//[Range(typeof(decimal), "0", "79228162514264337593543950335")  ]
		[Required]
		[DataType(DataType.Currency)]
		public decimal Amount { get; set; }

		[Required]
		public int CheckingAccountId { get; set; }
		public virtual CheckingAccount CheckingAccount { get; set; }
	}
}