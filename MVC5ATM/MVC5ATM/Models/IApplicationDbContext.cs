﻿using System.Data.Entity;

namespace MVC5ATM.Models
{
	public interface IApplicationDbContext
	{
		IDbSet<CheckingAccount> Checkingaccounts { get; set; }

		IDbSet<Transaction> Transactions { get; set; }

		int SaveChanges();
	}
}