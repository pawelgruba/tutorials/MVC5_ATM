﻿using MVC5ATM.Filters;
using System.Web;
using System.Web.Mvc;

namespace MVC5ATM
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			//filters.Add(new HandleErrorAttribute());

			//rejestracja atrybutu do wykonań globalnych (dla każdego żądania)
			filters.Add(new MVC5ATM.Filters.ActionExecutionLoger());

			//rejestracja nowego filtru dla błędów:
			filters.Add(new ExceptionFilter());
		}
	}
}
